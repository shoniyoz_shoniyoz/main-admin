import {createRouter, createWebHistory} from "vue-router";

function authGuard(to: any, from: any, next: any) {
  if (localStorage.getItem("token")) {
    next();
  } else {
    next("/login");
  }
}

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/login",
      name: "login",
      meta: {layout: "default"},
      component: () => import("../pages/login/Login.vue"),
    },
    {
      path: "/",
      name: "home",
      meta: {layout: "main"},
      component: () => import("../pages/home/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/delivery",
      name: "delivery",
      meta: {layout: "main"},
      component: () => import("../pages/delivery/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/booking",
      name: "booking",
      meta: {layout: "main"},
      component: () => import("../pages/booking/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/restaurant",
      name: "restaurant",
      meta: {layout: "main"},
      component: () => import("../pages/restaurant/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/clients",
      name: "clients",
      meta: {layout: "main"},
      component: () => import("../pages/clients/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/category-restaurant",
      name: "category-restaurant",
      meta: {layout: "main"},
      component: () => import("../pages/categoryRetaurant/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/popular-dishes",
      name: "popular-dishes",
      meta: {layout: "main"},
      component: () => import("../pages/popularDishes/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/cinema",
      name: "cinema",
      meta: {layout: "main"},
      component: () => import("../pages/cinema/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/club",
      name: "club",
      meta: {layout: "main"},
      component: () => import("../pages/club/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/club-category",
      name: "club-category",
      meta: {layout: "main"},
      component: () => import("../pages/clubCategory/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/courier",
      name: "courier",
      meta: {layout: "main"},
      component: () => import("../pages/courier/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/admin",
      name: "admin",
      meta: {layout: "main"},
      component: () => import("../pages/admin/Index.vue"),
      beforeEnter: authGuard
    },
  ],
});


export default router;
