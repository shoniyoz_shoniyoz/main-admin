import {defineStore} from "pinia";
import $axios from "@/plugins/axios";

export const useRestaurantStore = defineStore("restaurant", {
  state: () => ({
    restaurantType: [],
    restaurantAll: []
  }),
  actions: {
    async fetchRestaurantType() {
      try {
        this.restaurantType = await $axios.get('/type/')
      } catch (error) {
        return error
      }
    },
    async postRestaurant(form: Object) {
      try {
        await $axios.post('/restaurant', form)
      } catch (e) {
        return e
      }
    },
    async fetchRestaurant() {
      try {
        this.restaurantAll = await $axios.get('/restaurant/')
      } catch (error) {
        return error
      }
    },
    async deleteRestaurant(id: number) {
      try {
        await $axios.delete(`/restaurant/${id}`)
      } catch (error) {
        return error
      }
    },
    async restaurantStatus(id: number, status: boolean) {
      try {
        await $axios.put(`/restaurant/${id}/status`, {
          is_blocked: status
        })
      } catch (error) {
        return error
      }
    }
  },
});

