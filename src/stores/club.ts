import {defineStore} from "pinia";

export const useClubStore = defineStore("club", {
  state: () => ({}),
  actions: {
    async clubId(id: number) {
      localStorage.setItem('club_id', String(id))
    },
  },
});
